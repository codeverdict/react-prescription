import { Index_Prescription } from '../constant.js';

const prescription_detail = (action) => {
  return {
    details: action.details
  }
}

const prescriptions = (state=[], action) => {
  let prescriptions = null;
  switch(action.type){
    case Index_Prescription:
      prescriptions = [...state, prescription_detail(action)]
      return prescriptions;
    default:
      return state;
  }
}

export default prescriptions;