import {Index_Prescription} from '../constant.js';

export const index_prescription = (details) => {
  const action = {
    type: Index_Prescription,
    details: details
  }
  return action;
}