import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import '../app.css';
import { index_prescription } from '../actions/index.js';
import PrescriptionUpdate from './prescriptionUpdate.jsx';

class PrescriptionIndex extends Component {
  constructor(props){
    super(props);
    this.state={
      prescription: null,
      prescriptionInfo: null,
      loadId: false
    }
  }

  fetchprescriptions(){
    var thisValue = this
    const base_url= "http://192.168.1.22:3000/api/v1/";
    const fetch_url = `${base_url}/prescriptions`;
    console.log(fetch_url)
    fetch(fetch_url, {
      method: 'GET'
    })
    .then(response => response.json())
    .then(data =>{
      console.log('data', data)
      thisValue.setState({ prescription: data});
    })
  }

  componentWillMount(){
    this.fetchprescriptions()
  }

  presUpdate(details){
    var thisVal = this;
    this.setState({prescriptionInfo: details, loadId: true});
  }

  render(){
    let prescription = this.state.prescription;
    console.log("render pre index_prescription")
    return(
      <div className="row">
        <div className="col-md-12">
          <h3 className="app-title"> Prescription Index</h3>
            <div className="col-md-6">
            {
              prescription !== null ? (
                prescription.prescriptions.map(details => {
                  return(
                    <div key={details.id}>
                      <div>
                        <img src={details.image_path.image_path.thumb.url} 
                          className="pre-image"
                          alt="prescription" 
                          onClick={() => this.presUpdate(details)}
                        />
                      </div>
                    </div>  
                  )
                })
              ): <h3 className="null-case"> Prescription not added yet!</h3>
            }
          </div>
          <div className="col-md-6">
            { this.state.prescriptionInfo !== null ? (<div><PrescriptionUpdate preIndex={this} details={this.state.prescriptionInfo}/></div>): null}
          </div>
        </div>
      </div>    
    )
  }
}


function mapDispatchToProps(dispatch){
  return bindActionCreators({ index_prescription }, dispatch)
}

function mapStateToProps(state){
  return{
    prescriptions: state
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(PrescriptionIndex);