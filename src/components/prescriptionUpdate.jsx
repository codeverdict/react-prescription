import React, { Component } from 'react';
import '../app.css';


class PrescriptionUpdate extends Component{
  constructor(props){
    super(props);
    this.state={
     text: null,
    }
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount(){
    if (this.props.preIndex.state.loadId) {
      this.setState({text: this.props.details.text});
    }

  }

  componentDidUpdate(){
    if (this.props.preIndex.state.loadId) {
      this.setState({text: this.props.details.text});
      this.props.preIndex.setState({loadId: false});
    }

  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log("shouldComponentUpdate");
  //   var thisVal = this;
  //   console.log(nextProps.details.id !== this.props.details.id);
  //   if (nextProps.details.id !== this.props.details.id) {
  //     console.log(this.state.text);
  //     thisVal.setState({text: thisVal.props.details.text});
  //     return true;
  //   }

  // }

  updatePres(){
    const base_url= "http://192.168.1.22:3000/api/v1/";
    const fetch_url = `${base_url}/prescriptions/${this.props.details.id}/?text=${this.state.text}`;
    fetch(fetch_url, {
      method: 'PUT'
    }).then(response => {
      response.status === 200 ? alert('Prescription is updated'): null
    })
  }

  handleSubmit(event) {
    event.preventDefault();
  }

  render(){
    let details = this.props.details;
    console.log(details);
    console.log(this.state.text);
    return(
      <div className="text-update">
        <img src={details.image_path.image_path.url} className="pre-update-image" alt="prescription" />
        <br />
        <form onSubmit={this.handleSubmit} className="details-update form-group">
          <textarea type="text"
            value = {this.state.text}
            onChange={event => this.setState({text: event.target.value})}
            className="form-control details-text"
            rows="8"
          />
            <br />
          <button 
            onClick={() => this.updatePres()} 
            type="submit" className="btn btn-info btn-sm details-button">
            Submit
          </button>
        </form>  
      </div>
    )
  }
}

export default PrescriptionUpdate;