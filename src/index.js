import React from 'react';
import ReactDOM from 'react-dom';
import PrescriptionIndex from './components/prescriptionIndex.jsx';
import { createStore} from 'redux';
import reducer from './reducers';
import { Provider } from 'react-redux';

const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
    <PrescriptionIndex />
  </Provider>, document.getElementById('root')
) 